import tensorflow as tf
import numpy as np
from tensorflow_gmm2 import fnU


def fnTest():
    sess = tf.InteractiveSession()
    # test function
    fnTest = lambda tx, x: np.all(np.abs(x-tx.eval())<0.01)
    # test means (no prior)
    [nd,nx,ny,ng,nc] = [10000,1,1,2,5]
    a = np.linspace(0.1,0.9,ng)
    U = np.zeros([nx,ny,ng,nc])+np.reshape(np.linspace(0.1,0.9,ng),[1,1,ng,1])
    X = np.reshape(np.zeros([nd//ng,nx,ny,ng,nc])+ np.reshape(U,[1,nx,ny,ng,nc]),[-1,nx,ny,nc])+np.random.normal(0,0.1,[nd,nx,ny,nc])
    P = np.eye(ng)[np.reshape(np.zeros([nd//ng,nx,ny,ng],dtype=np.int)+ np.reshape(np.arange(ng),[1,1,1,ng]),[-1,nx,ny])]
    tx = tf.constant(X,dtype=tf.float32)
    tp = tf.constant(P,dtype=tf.float32)
    tu = fnU(tx,tp)
    assert(fnTest(tu,U))
    # test distance calculation
    d = np.square(np.expand_dims(a,-1)-np.expand_dims(a,0))*nc
    td = fnD(tx,tu)
    assert(fnTest(td[:ng,0,0,:],d))
    ts = fnS(tx,tp,tu)
    # test covariance (no prior)
    S = np.zeros([nx,ny,ng,nc,nc])
    tS = fnS(tx,tp,tu)
    assert(fnTest(tS,S))
    s = np.eye(5)
    S = np.tile(s,[nx,ny,ng,1,1])
    U = np.reshape(np.arange(ng),[1,1,1,ng,1])+np.zeros([nx,ny,1,nc])
    X = np.reshape(np.random.multivariate_normal(np.zeros(nc),s,[nd//ng,1,1,1])+np.reshape(U,[1,nx,ny,ng,nc]),[-1,nx,ny,nc])
    tx = tf.constant(X,dtype=tf.float32)
    tp = tf.constant(P,dtype=tf.float32)
    tu = tf.constant(U,tf.float32)
    tS = fnS(tx,tp,tu)
    assert(fnTest(tS/10,S/10))
    # test mahalanobis distance
    S = np.zeros([nx,ny,ng,nc,nc])+np.reshape(np.eye(nc),[1,1,1,nc,nc])*1
    tu = tf.constant(U,tf.float32)
    ts = tf.constant(S,dtype=tf.float32)
    tl = fnL(tx,tu,tS)
    assert(fnTest(tl[:ng,0,0,:],d))
    # test proportions
    S = np.zeros([nx,ny,ng,nc,nc])+np.reshape(np.eye(nc),[1,1,1,nc,nc])*0.00001
    tu = tf.constant(U,tf.float32)
    ts = tf.constant(S,dtype=tf.float32)
    tl = fnL(tx,tu,ts)
    tp = fnP(tl)
    assert(fnTest(tp,P))
    # test some loops
    [nd, nx, ny, ng, nc] = [10000, 1, 1, 2, 5]
    s = np.eye(nc)
    S = np.tile(s,[nx,ny,ng,1,1])
    U = np.reshape(np.arange(ng),[1,1,ng,1])+np.zeros([nx,ny,1,nc])
    X = np.reshape(np.random.multivariate_normal(np.zeros(nc),s,[nd//ng,1,1,1])+np.reshape(U,[1,nx,ny,ng,nc]),[-1,nx,ny,nc])
    P = np.eye(ng)[np.reshape(np.zeros([nd//ng,nx,ny,ng],dtype=np.int)+ np.reshape(np.arange(ng),[1,1,1,ng]),[-1,nx,ny])]
    tx = tf.constant(X,dtype=tf.float32)
    tp = tf.constant(P,dtype=tf.float32)
    tu = tf.constant(U,tf.float32)
    tS = tf.constant(S,tf.float32)
    for i in range(20):
        tl = fnL(tx,tu,tS)
        print("Entropy: {:06f}, Total NLL: {:06f}".format(fnHC(tl).eval(), fnDC(tl).eval()))
        tp = fnP(tl)
        tu = fnU(tx, tp)
        tS = fnS(tx, tp, tu)
    tl,tu,tS = fnSort(tl, tu, tS)

    # better test
    [nd, nx, ny, ng, nc] = [100, 1, 1, 4, 5]
    p = np.repeat(np.arange(0,ng),np.arange(1,ng+1))
    s = np.repeat(np.arange(1,ng+1),np.arange(1,ng+1))
    u = np.repeat(np.cumsum(np.arange(0,ng)),np.arange(1,ng+1))
    X = np.random.multivariate_normal(np.zeros(nc),np.eye(nc),[nd,nx,ny,len(s)])*np.reshape(s/10,[1,1,1,-1,1])+np.reshape(u,[1,1,1,-1,1])
    p = np.eye(ng)[np.repeat(np.arange(0,ng),np.arange(1,ng+1))]
    tx = tf.reshape(tf.constant(X,tf.float32),[-1,nx,ny,nc])
    #tu = tf.tile(tf.constant(u,tf.float32),[])
    #tS = tf.constant(s,tf.float32)

    sess.close()
