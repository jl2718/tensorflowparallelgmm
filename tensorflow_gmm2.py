#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import tensorflow as tf
import os
import matplotlib.pyplot as plt
prior = 1.0 # strength of population prior for mean and covariance
eps = 1e-12 # numerical stability constant for divisions

def fnInit(tx,ng): # TODO: pre-factorize X=QR and compute |Q><Q| because <X|(I/S)|X> = |Q><Q|(W/tr(W)-ww/w.w)|Q><Q| where we care only about the diagonal
    [nd,nx,ny,nc] = tx.shape.as_list()
    # tq,tr = tf.qr(tf.transpose(tx,[1,2,0,3]))
    ta = tf.reshape(tx,[-1,nc])
    tb = tf.reduce_mean(tx,[0,1,2])
    ta = ta - tb
    tA = (tf.matmul(ta,ta,True)/tf.cast(tf.shape(ta)[0],tf.float32))
    #tAL = tf.cholesky(tA)
    #tl = tf.nn.softmax(tf.log(tf.random_uniform(shape=tf.concat([tf.shape(tx)[:-1],tf.constant([ng])],0))+eps),-1)
    tp = tf.random_uniform(shape=tf.concat([tf.shape(tx)[:-1],tf.constant([ng])],0))
    tp = tp / tf.reduce_sum(tp,-1,True) # simpler initializer
    vu = tf.Variable(initial_value = tf.random_uniform([nx,ny,ng,nc]))# cluster centers
    vS = tf.Variable(initial_value = tf.tile(tf.reshape(tA,[1,1,1,nc,nc]),[nx,ny,ng,1,1]))# cluster covariance
    vc = tf.Variable(initial_value = np.log(ng),dtype=tf.float32) # init to max entropy
    vA = tf.Variable(initial_value = tf.reshape(tA,[1,1,1,nc,nc])) # population covariance prior
    vb = tf.Variable(initial_value= tf.reshape(tb,[1,1,nc])) # population mean prior
    return(vu,vS,vc,vA,vb,tp)

def fnAssign(tu,tS,tc):
    tu = vu.assign(tu)
    tS = vS.assign(tS)
    tc = vc.assign(tc)
    return(tu,tS,tc)

def fnU(tx,tp):
    [nd, nx, ny, nc] = tx.shape.as_list()
    ty = tf.multiply(tf.expand_dims(tx,-2),tf.expand_dims(tp,-1)) # separate into mixture components
    tw = tf.reduce_sum(tp,0) # total normalized probability weight in each group
    #tu = (tf.reduce_sum(ty,0) + (prior * vb)) / (tf.expand_dims(tw,-1)+prior) # group weighted means
    tu = (tf.reduce_sum(ty, 0)+eps) / (tf.expand_dims(tw, -1)+eps)  # group weighted means
    return(tu)

def fnS(tx,tp,tu): # TODO: use L from Cholesky as parameter instead of S
    [nd, nx, ny, nc] = tx.shape.as_list()
    ng = tu.shape.as_list()[-2]
    te = tf.expand_dims(tf.expand_dims(tx, -2) - tu, -1) # group error vectors
    tE = tf.reduce_sum(tf.matmul(te,te,False,True) * tf.reshape(tp,[-1,nx,ny,ng,1,1]),0)
    tS = (tE + prior * vA) / (tf.reshape(tf.reduce_sum(tp,0),[nx,ny,ng,1,1]) + prior)
    #tL = tf.cholesky(tS)  # L where LL' = S
    # tS = (tE) / (tf.reshape(tf.reduce_sum(tp,0),[nx,ny,ng,1,1]))
    return(tS)

def fnL(tx,tu,tS): # negative log likelihood (mahalanobis distance)
    [nd, nx, ny, nc] = tx.shape.as_list()
    te = tf.subtract(tf.expand_dims(tx,-2),tu) # error e=(x-u) for each x,group
    tL = tf.cholesky(tS) # L where LL' = S
    ty = tf.matrix_triangular_solve(tL,tf.transpose(te,[1,2,3,4,0])) # Y where LY=E
    tl = tf.transpose(tf.reduce_sum(tf.square(ty),-2),[3,0,1,2])# diag(Y'Y)
    return(tl)

def fnD(tx,tu): # squared euclidean distance
    te = tf.expand_dims(tf.subtract(tf.expand_dims(tx, -2), tu),-1)  # error e=(x-u) for each x,group
    td = tf.squeeze(tf.matmul(te,te,True),[-1,-2])
    return(td)

def fnP(tl): # normalized posterior probability from negative log likelihood (distance)
    # Dempster formula
    ta = tf.exp(-tl) + eps # get group likelihood (eps is uninformative prior for numerical stability)
    tb = (ta*tf.reduce_mean(ta,0,keep_dims=True)) # apply group prior
    tp = tb / tf.reduce_sum(tb,-1,keep_dims=True) # normalize
    #tp = tf.nn.softmax(-tl)  # group membership likelihood (normalized)
    #tp = tf.exp(-tl)
    #tp = tf.one_hot(tf.argmin(tl, -1), ng, on_value=1.0, off_value=0.0)  # one-hot encoding of group
    return(tp)

def fnSort(tp,tl,tu,tS): # sort by group dominance
    [nx,ny,ng,nc] = tu.shape.as_list()
    _ , ti = tf.nn.top_k(tf.reduce_sum(tp,0),k=ng)
    tk = tf.one_hot(ti,ng,1.0,0.0)
    tu = tf.reduce_sum(tf.multiply(tf.expand_dims(tk,-1),tf.expand_dims(tu,-3)),-2)
    tS = tf.reduce_sum(tf.multiply(tf.expand_dims(tf.expand_dims(tk,-1),-1),tf.expand_dims(tS,-4)),-3)
    tp = tf.transpose(tf.matmul(tk, tf.transpose(tp, [1, 2, 3, 0])), [3, 0, 1, 2])
    tl = tf.transpose(tf.matmul(tk, tf.transpose(tl, [1, 2, 3, 0])), [3, 0, 1, 2])
    return(tp,tl,tu,tS)

def fnHC(tl,tp): # compute entropy cost of mixture model
    tc = tf.reduce_mean(tf.reduce_sum(tl*tp,-1)) # total entropy
    return(tc)

def fnDC(td): # compute entropy cost of discrete model
    tc = tf.reduce_mean(tf.reduce_min(td,-1))
    return(tc)

def fnKM(tx,tp,td,tloop): # k-means iteration
    pass # TODO

def fnLoop(tx,tl,tp,tc,ti,tloop):
    return(tloop)

def fnMinU(tx,tl,tc,tu):
    tc = vc.assign(fnHC(tl))
    tp = fnP(tl)
    tu = vu.assign(fnU(tx, tp))
    tl = fnL(tx,tu,vS)
    return(tx,tl,tc,tu)

def fnMinS(tx,tl,tc,tS):
    tc = vc.assign(fnHC(tl))
    tp = fnP(tl)
    tS = vS.assign(fnS(tx,tp,tu))
    tl = fnL(tx,vu,tS)
    return(tx,tl,tc,tS)

def fnEM(tx,tl,tp,tc,ti,tloop): #Expectation maximization
    ti = tf.add(ti, 1)
    tc0 = vc.assign(tc)
    tu = vu.assign(fnU(tx, tp))
    tS = vS.assign(fnS(tx, tp, tu))
    tl = fnL(tx, tu, tS)
    tp = fnP(tl)
    tc = fnHC(tl,tp)
    tloop = tf.logical_and(tf.not_equal(tc,tc0),tf.less(ti,100))
    return(tx,tl,tp,tc,ti,tloop)

def fnFG(tx,tu,tS): # get foreground images
    tl = fnL(tx,tu,tS)
    # tb = tf.one_hot(tf.argmin(tl,-1),tu.shape.as_list()[-2],1.0,0.0,-1)[:,:,:,0]
    # tb = tf.cast(tf.nn.softmax(-tl, -1)[:, :, :, 0]<0.01,tf.float32)
    # tb = fnP(tl)[:,:,:,0]
    #tb = tf.cast(tl[:,:,:,0]<0.3,tf.float32)
    tb = tf.cast(tl[:, :, :, 0] < tf.contrib.distributions.percentile(tl[:, :, :, 0], 0.9, 0, keep_dims=True),tf.float32)
    tz = tf.add(
        tf.multiply(tf.expand_dims(1-tb, -1), tx),
        tf.multiply(tf.expand_dims(tb, -1), tf.fill(tf.shape(tx), 0.5))
    )
    return(tz)

fnNormProb = lambda x,axis=0:x/np.sum(x,axis)
fnNormLL = lambda x,axis=0:fnNormProb(np.exp(x-np.max(x,axis)),axis)


imgdir = './C001'
(nb,nx,ny,nc,ng) = (16,720,1280,3,2) # size of (img_x,img_y,img_channels,groups)
fnImgParse = lambda filename:tf.divide(tf.cast(tf.image.resize_image_with_crop_or_pad(tf.image.decode_jpeg(tf.read_file(filename),channels=nc),nx,ny),tf.float32),tf.constant(255.,dtype=tf.float32))
#fnImgParse = lambda filename:tf.divide(tf.cast(tf.image.decode_image(tf.read_file(filename)),tf.float32),tf.constant(255.,dtype=tf.float32))
tf.reset_default_graph()
sess = tf.InteractiveSession()
t_files = tf.constant([r+'/'+f for r,d,ff in os.walk(imgdir) for f in ff if '.jpg' in f],dtype=tf.string)
tx = tf.contrib.data.Dataset.from_tensor_slices(t_files).map(fnImgParse).batch(nb).repeat().make_one_shot_iterator().get_next()
x = tx.eval()
tx = tf.constant(x)
(vu,vS,vc,vA,vb,tp) = fnInit(tx,ng)
sess.run(tf.global_variables_initializer()) # also loads data
tl = -tf.log(tp) #placeholder
# Tests
#tu = fnU(tx,tp)
#tS = fnS(tx,tp,tu)
#tl = fnL(tx,vu,vS)
#tp = fnP(tl)
#tstop  = fnLoop(tx, tl,tp,vc, tf.constant(0),tf.constant(True)) # test loop condition
#tx,tl,tp,tc,ti,tstop  = fnEM(tx, tl,tp,vc, tf.constant(0),tf.constant(True)) # test EM loop
tx,tl,tp,tc,ti,tloop  = tf.while_loop(fnLoop, fnEM, [tx, tl,tp,vc, tf.constant(0),tf.constant(True)])
tp,tl,tu,tS = fnSort(tp,tl,vu,vS)
x,l,p,u,S,c,i = sess.run([tx,tl,tp,tu,tS,tc,ti])
print("{:d} iterations".format(i))
# b = np.expand_dims(p[:,:,:,0],-1)>0.0001#(0.1/(ng+1))
#b = np.expand_dims(l[:,:,:,0]<np.percentile(l[:,:,:,0],80,0,keepdims=True),-1) # np.percentile(np.reshape(l[:,:,:,0],[-1]),90)
import scipy
s = np.zeros_like(l)
for i in range(len(l)):
    s[i] = scipy.ndimage.filters.gaussian_filter(l[i],(1,1,0),0)

b = np.expand_dims(s[:,:,:,0]<np.percentile(np.reshape(s[:,:,:,0],[-1]),80),-1)
# b = np.expand_dims(l[:,:,:,1]>np.percentile(l[:,:,:,1],10,0,keepdims=True),-1)
z = (1-b)*x + np.ones_like(x)*0.5*(b)
for i in range(ng): # save means to image
    plt.imsave("u{:02d}.jpg".format(i),np.squeeze(u[:,:,i,:]))
for i in range(len(z)): # save foreground to image
    plt.imsave("fg_{:02d}.jpg".format(i),z[i])
for i in range(len(p)): # save background posterior probability (acc to Demster) to image
    plt.imsave("pp_{:02d}.jpg".format(i), p[i,:,:,0],cmap='inferno')
for i in range(len(p)): # save background log likelihood to image
    plt.imsave("ll_{:02d}.jpg".format(i), l[i, :, :, 0], cmap='inferno')
#sess.close()